# r-packages-list

List of R packages, required by some services.
The packages can be sources from  CRAN-like repositories, sources from github, or - preferred - packages in deb format.

## install2r-packages.txt

That file is used by the Dockerfile that builds the Rstudio container images. Packages listed there are installed from a binary repository to speed up the installation procedure and preserve disk space.

The file jupyter-image-r-cran-pkgs.txt contains packages that are installed into the docker image from CRAN sources.
